package ufps.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Letra {

    private List<short[]> posiciones;
    private Character letra;
    private boolean esDerecho;

    public Letra(Character letra, boolean esDerecho, short posicionX, short posicionY) {
        posiciones = new ArrayList<>();
        posiciones.add(new short[]{posicionX, posicionY});
        this.esDerecho = esDerecho;
        this.letra = letra;
    }

    public Letra(Character letra, boolean esDerecho) {
        this.letra = letra;
        this.esDerecho = esDerecho;
    }

    public List<short[]> getPosiciones() {
        return posiciones;
    }

    public void setPosiciones(List<short[]> posiciones) {
        this.posiciones = posiciones;
    }

    public boolean isEsDerecho() {
        return esDerecho;
    }

    public void setEsDerecho(boolean esDerecho) {
        this.esDerecho = esDerecho;
    }

    public Character getLetra() {
        return letra;
    }

    public void setLetra(Character letra) {
        this.letra = letra;
    }

    public void addPosicion(short posicionX, short posicionY) {
        posiciones.add(new short[]{posicionX, posicionY});
    }

    @Override
    public String toString() {
        if (this.letra == '@') { // RAIZ
            return "   @";
        } else if (this.letra == '$') { // NODOS VACIOS
            return this.esDerecho ? "   1" : "   0";
        } else {
            String texto = this.esDerecho ? "1: " : "0: ";

            switch (this.letra) {
                case '/':
                    texto += "\\n";
                    break;
                case '#':
                    texto += "ch";
                    break;
                default:
                    texto += letra.toString();
            }

            texto += ": \n";

            for (int i = 0; i < posiciones.size(); i++) {
                short[] pos = posiciones.get(i);
                texto += "[" + pos[0] + "," + pos[1] + "]";
                if (i < posiciones.size() - 1) {
                    texto += "\n";
                }
            }

            return texto;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.letra);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Letra other = (Letra) obj;
        return Objects.equals(this.letra, other.letra);
    }

}
