package ufps.util.colecciones_seed;

import ufps.util.Diccionario;
import ufps.util.Letra;

public class ArbolBinarioDiccionario<T> extends ArbolBinario<T> {

    Diccionario diccionario = new Diccionario();

    public ArbolBinarioDiccionario() {
        super();
    }

    public ArbolBinarioDiccionario(T raiz) {
        super(raiz);
    }

    public void llenarArbol(String linea, short posicionY) {

        short posicionX = 0;
        NodoBin<T> nodoExiste;
        char[] letras = linea.toCharArray();
        char letra;

        for (short i = 0; i < letras.length; i++) {

            if (isCh(i, letras)) {
                letra = '#';
                i++;
            } else {
                letra = letras[i];
            }

            nodoExiste = this.buscarNodo(letra);
            if (nodoExiste == null) {

                boolean[] binarios = diccionario.buscar(letra);
                NodoBin<T> nodo = super.raiz;
                boolean derecho = false;

                for (boolean binario : binarios) {
                    if (!binario) { // 0
                        if (nodo.getIzq() == null) {
                            nodo.setIzq(new NodoBin<>((T) new Letra('$', false)));
                        }
                        nodo = nodo.getIzq();
                        derecho = false;
                    } else { // 1
                        if (nodo.getDer() == null) {
                            nodo.setDer(new NodoBin<>((T) new Letra('$', true)));
                        }
                        nodo = nodo.getDer();
                        derecho = true;
                    }
                }
                nodo.setInfo((T) new Letra(letra, derecho, posicionX, posicionY));
            } else {
                Letra info = (Letra) nodoExiste.getInfo();
                info.addPosicion(posicionX, posicionY);
                nodoExiste.setInfo((T) info);
            }

            posicionX++;
        }

    }

    private boolean isCh(short posicion, char[] letras) {

        try {
            if (letras[posicion] == 'c' && letras[posicion + 1] == 'h') {
                return true;
            }
        } catch (Exception e) {
        }

        return false;
    }

    public ArbolExpresionGrafico getDibujo() {
        ArbolExpresionGrafico panel = new ArbolExpresionGrafico(this);

        System.out.println("ancho: " + panel.getAncho());

        return panel;
    }

    public int buscarCoincidencias(String palabra) {

        palabra = formatearTexto(palabra);

        char[] letras = palabra.toCharArray();
        int total = 0;

        try {
            Letra primeraLetra = (Letra) buscarNodo(letras[0]).getInfo();

            for (short[] posiciones : primeraLetra.getPosiciones()) {

                total += buscarCoincidencias(letras, 1, posiciones[0] + 1, posiciones[1]) ? 1 : 0;

            }

        } catch (Exception e) {
        }

        return total;
    }

    private String formatearTexto(String texto) {
        String formato = texto.toLowerCase();

        return formato.replace("ch", "#");
    }

    private boolean buscarCoincidencias(char[] letras, int posicion, int posicionX, int posicionY) {

        if (letras.length == posicion) {
            return true;
        }

        NodoBin<T> nodo = buscarNodo(letras[posicion]);

        if (nodo == null) {
            return false;
        }

        Letra letra = (Letra) nodo.getInfo();

        for (short[] posiciones : letra.getPosiciones()) {
            if (posiciones[0] == posicionX && posiciones[1] == posicionY) {
                return buscarCoincidencias(letras, posicion + 1, posicionX + 1, posicionY);
            }
        }
        return false;

    }

    private NodoBin<T> buscarNodo(char letra) {
        try {
            boolean[] binarios = diccionario.buscar(letra);
            NodoBin<T> nodo = super.raiz;

            for (boolean binario : binarios) {
                if (!binario) { // 0
                    nodo = nodo.getIzq();
                } else { // 1
                    nodo = nodo.getDer();
                }
            }
            return nodo;

        } catch (Exception e) {
        }

        return null;
    }

}
